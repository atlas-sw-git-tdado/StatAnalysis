# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building/installing python "analysis" modules.
#

# The name of the package:
atlas_subdir( PyAnalysis )

# Figure out whether we need to do anything.
if( NOT ATLAS_BUILD_PYANALYSIS )
   return()
endif()

# Tell the user what's happening:
message( STATUS "BUILDING: setuptools, Cython, numpy, pip, wheel (core extensions of Python)" )

# Figure out where to take Python from:
if( ATLAS_BUILD_PYTHON )
   set( Python_EXECUTABLE $<TARGET_FILE:Python::Interpreter> )
   set( Python_VERSION_MAJOR 3 )
   set( Python_VERSION_MINOR 10 )
   if(NOT TARGET libffi::ffi)
      find_package( libffi QUIET )
   endif()
else()
   find_package( Python COMPONENTS Interpreter REQUIRED )
   find_package( libffi REQUIRED )
endif()

if (NOT STATANA_VERBOSE)
   set(_logging "LOG_DOWNLOAD ON LOG_CONFIGURE ON LOG_BUILD ON LOG_INSTALL ON")
endif()

# Install the find module(s):
install( FILES cmake/FindNumPy.cmake
        DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )

# Setup the build/runtime environment for the python analysis packages:
configure_file(
        ${CMAKE_CURRENT_SOURCE_DIR}/cmake/PyAnalysisEnvironmentConfig.cmake.in
        ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PyAnalysisEnvironmentConfig.cmake
        @ONLY )
set( PyAnalysisEnvironment_DIR
        ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}
        CACHE PATH "Location of PyAnalysisEnvironmentConfig.cmake" )
find_package( PyAnalysisEnvironment REQUIRED )

# A common installation directory for all python externals of the package:
set( _buildDir
        ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PyAnalysisBuild )
set( _sitePkgsDir ${_buildDir}/lib/python${Python_VERSION_MAJOR}.${Python_VERSION_MINOR}/site-packages )

# Build/install setuptools:
set( _source
        "http://cern.ch/lcgpackages/tarFiles/sources/setuptools-44.1.0.zip" )
set( _md5 "f17522189e7c0262b67e590d529d4e06" )
ExternalProject_Add( setuptools
        PREFIX ${CMAKE_BINARY_DIR}
        INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
        URL ${_source}
        URL_MD5 ${_md5}
        BUILD_IN_SOURCE 1
        LOG_DOWNLOAD ON LOG_CONFIGURE ON LOG_BUILD ON LOG_INSTALL ON
        CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory ${_sitePkgsDir}
        COMMAND ${CMAKE_COMMAND} -E env
        LD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
        DYLD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
        ${Python_EXECUTABLE} <SOURCE_DIR>/bootstrap.py
        BUILD_COMMAND ${CMAKE_COMMAND} -E env
        LD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
        DYLD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
        ${Python_EXECUTABLE} <SOURCE_DIR>/setup.py build
        INSTALL_COMMAND ${CMAKE_COMMAND} -E env PYTHONPATH=${_sitePkgsDir}
        LD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
        DYLD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
        ${Python_EXECUTABLE} <SOURCE_DIR>/setup.py install --prefix=${_buildDir}
        COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/../atlasexternals/External/scripts/sanitizePythonScripts.sh
        "${_buildDir}/bin/easy_install*"
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR> )
add_dependencies( Package_PyAnalysis setuptools )
if( ATLAS_BUILD_PYTHON )
   add_dependencies( setuptools Python )
endif()

if( NOT NUMPY_FOUND OR ATLAS_BUILD_PYTHON )

   # Build/install Cython.
   set( _source
           "https://cern.ch/lcgpackages/tarFiles/sources/Cython-0.29.28.tar.gz" )
   set( _md5 "0e98543dca816300a27e7d76146a6280" )
   ExternalProject_Add( Cython
           PREFIX ${CMAKE_BINARY_DIR}
           INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
           URL ${_source}
           URL_MD5 ${_md5}
           BUILD_IN_SOURCE 1
           LOG_DOWNLOAD ON LOG_CONFIGURE ON LOG_BUILD ON LOG_INSTALL ON
           CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory ${_sitePkgsDir}
           BUILD_COMMAND ${CMAKE_COMMAND} -E env PYTHONPATH=${_sitePkgsDir}
           LD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           DYLD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           ${Python_EXECUTABLE} <SOURCE_DIR>/setup.py build
           INSTALL_COMMAND ${CMAKE_COMMAND} -E env PYTHONPATH=${_sitePkgsDir}
           LD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           DYLD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           ${Python_EXECUTABLE} <SOURCE_DIR>/setup.py install --prefix=${_buildDir}
           COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR> )
   add_dependencies( Package_PyAnalysis Cython )
   add_dependencies( Cython setuptools )

   # Build/install numpy:
   set( _source
           "http://cern.ch/lcgpackages/tarFiles/sources/numpy-1.22.3.tar.gz" )
   set( _md5 "3305c27e5bdf7f19247a7eee00ac053e" )
   ExternalProject_Add( numpy
           PREFIX ${CMAKE_BINARY_DIR}
           INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
           URL ${_source}
           URL_MD5 ${_md5}
           BUILD_IN_SOURCE 1
           LOG_DOWNLOAD ON LOG_CONFIGURE ON LOG_BUILD ON LOG_INSTALL ON
           CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory ${_sitePkgsDir}
           BUILD_COMMAND ${CMAKE_COMMAND} -E env --unset=SHELL
           PYTHONPATH=${_sitePkgsDir}
           LD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           DYLD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           ${Python_EXECUTABLE} <SOURCE_DIR>/setup.py build
           INSTALL_COMMAND ${CMAKE_COMMAND} -E env --unset=SHELL
           PYTHONPATH=${_sitePkgsDir}
           LD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           DYLD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           ${Python_EXECUTABLE} <SOURCE_DIR>/setup.py install --prefix=${_buildDir}
           COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/../atlasexternals/External/scripts/sanitizePythonScripts.sh
           "${_buildDir}/bin/f2py*"
           COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR> )
   add_dependencies( Package_PyAnalysis numpy )
   add_dependencies( numpy setuptools Cython )
endif()

if( NOT PIP_FOUND OR ATLAS_BUILD_PYTHON )
   # Build/install wheel:
   set( _source
           "http://cern.ch/lcgpackages/tarFiles/sources/wheel-0.33.4.tar.gz" )
   set( _md5 "698ac73e028f3a9084dd6c68b2713faa" )
   ExternalProject_Add( wheel
           PREFIX ${CMAKE_BINARY_DIR}
           INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
           URL ${_source}
           URL_MD5 ${_md5}
           BUILD_IN_SOURCE 1
           LOG_DOWNLOAD ON LOG_CONFIGURE ON LOG_BUILD ON LOG_INSTALL ON
           CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory ${_sitePkgsDir}
           BUILD_COMMAND ${CMAKE_COMMAND} -E env PYTHONPATH=${_sitePkgsDir}
           LD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           DYLD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           ${Python_EXECUTABLE} <SOURCE_DIR>/setup.py build
           INSTALL_COMMAND ${CMAKE_COMMAND} -E env PYTHONPATH=${_sitePkgsDir}
           LD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           DYLD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           ${Python_EXECUTABLE} <SOURCE_DIR>/setup.py install --prefix=${_buildDir}
           COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/../atlasexternals/External/scripts/sanitizePythonScripts.sh
           "${_buildDir}/bin/wheel*"
           COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR> )
   add_dependencies( Package_PyAnalysis wheel )
   add_dependencies( wheel setuptools )
   if( ATLAS_BUILD_PYTHON )
      add_dependencies( wheel Python )
   endif()
   # This makes sure that package compilation would happen one-by-one. Otherwise
   # some race conditions can arise when installing packages into the same area
   # at once.
   if( NOT NUMPY_FOUND OR ATLAS_BUILD_PYTHON )
      add_dependencies( wheel numpy )
   endif()

   # Build/install pip:
   set( _source
           "http://cern.ch/lcgpackages/tarFiles/sources/pip-21.0.1.tar.gz" )
   set( _md5 "246523bd34dd356e7506adf54d206b12" )
   ExternalProject_Add( pip
           PREFIX ${CMAKE_BINARY_DIR}
           INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
           URL ${_source}
           URL_MD5 ${_md5}
           BUILD_IN_SOURCE 1
           LOG_DOWNLOAD ON LOG_CONFIGURE ON LOG_BUILD ON LOG_INSTALL ON
           CONFIGURE_COMMAND ${CMAKE_COMMAND} -E make_directory ${_sitePkgsDir}
           BUILD_COMMAND ${CMAKE_COMMAND} -E env PYTHONPATH=${_sitePkgsDir}
           LD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           DYLD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           ${Python_EXECUTABLE} <SOURCE_DIR>/setup.py build
           INSTALL_COMMAND ${CMAKE_COMMAND} -E env PYTHONPATH=${_sitePkgsDir}
           LD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           DYLD_LIBRARY_PATH=$<TARGET_FILE_DIR:libffi::ffi>
           ${Python_EXECUTABLE} <SOURCE_DIR>/setup.py install --prefix=${_buildDir}
           COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/../atlasexternals/External/scripts/sanitizePythonScripts.sh
           "${_buildDir}/bin/pip*"
           COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR> )
   add_dependencies( Package_PyAnalysis pip )
   add_dependencies( pip setuptools wheel )
   if( ATLAS_BUILD_PYTHON )
      add_dependencies( pip Python )
   endif()
endif()

# Set up a dummy imported target that other packages could use to find these
# python modules in their own builds.
add_library( PyAnalysis::PyAnalysis UNKNOWN IMPORTED GLOBAL )
set_target_properties( PyAnalysis::PyAnalysis PROPERTIES
        INSTALL_PATH "${_buildDir}"
        BINARY_PATH "${_buildDir}/bin"
        PYTHON_PATH "${_sitePkgsDir}" )

# Install all built modules at the same time:
install( DIRECTORY ${_buildDir}/
        DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Test that, whatever the setup, we are able to use pip to install (as an
# example) the cookiecutter project.
set( _pipInstallDir
        ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/cookiecutterTest )
atlas_add_test( pip_install
        SCRIPT pip install --target=${_pipInstallDir} cookiecutter )

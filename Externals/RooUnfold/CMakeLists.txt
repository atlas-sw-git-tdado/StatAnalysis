# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Package building RooUnfold for StatAnalysis releases
#

# The name of the package:
atlas_subdir( RooUnfold )

set( STATANA_ROOUNFOLD_REPOSITORY "${CERN_GITLAB_PREFIX}/RooUnfold/RooUnfold.git" CACHE STRING "Repository of RooUnfold" )


set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/RooUnfoldBuild )

if(STATANA_VERBOSE)
    set(_logging OFF)
else()
    set(_logging ON)
endif()

# Build lwtnn for the build area:
ExternalProject_Add( RooUnfold
        PREFIX ${CMAKE_BINARY_DIR}
        INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
        GIT_REPOSITORY ${STATANA_ROOUNFOLD_REPOSITORY}
        GIT_TAG ${STATANA_ROOUNFOLD_VERSION}
	BUILD_ALWAYS ${TRACK_CHANGES}
        CMAKE_CACHE_ARGS
        -DRooUnfoldGenerateCMakeConfig:BOOL=OFF	
        -DRooUnfoldTests:BOOL=OFF
        -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
        -DCMAKE_PREFIX_PATH:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}  # ensures will discover the release's version of ROOT
        LOG_DOWNLOAD ${_logging} LOG_CONFIGURE ${_logging} LOG_BUILD ${_logging} LOG_INSTALL ${_logging}
        UPDATE_COMMAND "" # needed for next line to work
        UPDATE_DISCONNECTED TRUE) # skips reconfigure+build if just rerunning.
ExternalProject_Add_Step( RooUnfold buildinstall
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir} <INSTALL_DIR>
        COMMENT "Installing RooUnfold into the build area"
        DEPENDEES install
        )
add_dependencies( Package_RooUnfold RooUnfold )

if( ATLAS_BUILD_ROOT )
    add_dependencies ( RooUnfold ROOT )
endif()


# Install RooUnfold:
install( DIRECTORY ${_buildDir}/
        DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )


# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building Boost as part of the offline / analysis release.
#

# Set the name of the package:
atlas_subdir( Boost )

# In release recompilation mode stop now:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Stop if the build is not needed:
if( NOT ATLAS_BUILD_BOOST )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building Boost as part of this project" )

# The source code of Boost:
set( _boostSource
   "http://cern.ch/lcgpackages/tarFiles/sources/boost_1_75_0.tar.gz" )
set( _boostMd5 "38813f6feb40387dfe90160debd71251" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/BoostBuild )

# Use all available cores for the build:
atlas_cpu_cores( nCPUs )

# Decide where to take Python from:
if( ATLAS_BUILD_PYTHON )
   set( Python_EXECUTABLE "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/python3" )
   set( Python_VERSION "3.10" )
   set( Python_INCLUDE_DIR "${CMAKE_INCLUDE_OUTPUT_DIRECTORY}/python3.10" )
   set( Python_LIBRARY_DIR "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}" )
else()
   find_package( Python COMPONENTS Interpreter Development )
   list( GET Python_INCLUDE_DIRS 0 Python_INCLUDE_DIR )
   list( GET Python_LIBRARY_DIRS 0 Python_LIBRARY_DIR )
endif()
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/user-config.jam.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/user-config.jam.pre
   @ONLY )
file( GENERATE
   OUTPUT ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/user-config.jam
   INPUT ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/user-config.jam.pre )

# Only add debug symbols in Debug build mode:
set( _extraOpt )
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
   list( APPEND _extraOpt "variant=debug" )
else()
   list( APPEND _extraOpt "variant=release" )
endif()
# Set a compiler toolset explicitly. For the cases where multiple compiler
# types are available in the environment at the same time.
if( "${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang" )
   list( APPEND _extraOpt "toolset=clang" )
elseif( "${CMAKE_CXX_COMPILER_ID}" MATCHES "GNU" )
   list( APPEND _extraOpt "toolset=gcc" )
endif()
# Set the C++ standard for the build explicitly. Only considering C++14 and
# C++17 for the moment...
if( "${CMAKE_CXX_STANDARD}" EQUAL 14 )
   list( APPEND _extraOpt "cxxflags=-std=c++14" )
elseif( "${CMAKE_CXX_STANDARD}" EQUAL 17 )
   list( APPEND _extraOpt "cxxflags=-std=c++17" )
endif()

# Create the helper scripts.
string( REPLACE ";" " " _configureOpt "${_extraOpt}" )
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/configure.sh.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh @ONLY )
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/build.sh.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/build.sh @ONLY )
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/install.sh.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/install.sh @ONLY )

# Build Boost:
ExternalProject_Add( Boost
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_boostSource}
   URL_MD5 ${_boostMd5}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh
   COMMAND ${CMAKE_COMMAND} -E copy
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/user-config.jam
   <BINARY_DIR>/tools/build/src/
   BUILD_COMMAND
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/build.sh
   INSTALL_COMMAND
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/install.sh
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/
   <INSTALL_DIR>
   LOG_DOWNLOAD ON LOG_CONFIGURE ON LOG_BUILD ON LOG_INSTALL ON LOG_OUTPUT_ON_FAILURE ON )
ExternalProject_Add_Step( Boost forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of Boost"
   DEPENDERS download )
add_dependencies( Package_Boost Boost )

# If we are building Python ourselves, make Boost depend on it:
if( ATLAS_BUILD_PYTHON )
   add_dependencies( Boost Python )
endif()

# Install Boost:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

software            included ref    included ref date    latest ref    latest ref date
------------------  --------------  -------------------  ------------  -----------------
BOOTSTRAPGENERATOR  71e6ebe2
COMMONSTATTOOLS     74133b45
HISTFITTER          v1.0.0
PYHF                0.7.0
QUICKFIT            df48d3
ROOFITEXTENSIONS    2e5925e1
ROOT                v6-26-06
ROOUNFOLD           924c8b60
SIGNIFICANCE        c6192bc
TREXFITTER          6070f4e4
WORKSPACECOMBINER   8c0a7050
XMLANAWSBUILDER     4b0d11f8
XROOFIT             8fe5a25e